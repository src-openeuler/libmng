Name:          libmng
Version:       2.0.3
Release:       10
Summary:       Used for supporting Multiple-image Network Graphics
License:       zlib
URL:           http://www.libmng.com/
Source0:       http://download.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
BuildRequires: autoconf lcms2-devel libjpeg-turbo-devel libtool zlib-devel

%description
LibMNG is a library which uesed for accessing graphics in Multi-image Network
Graphics and JPEG Network Graphics formats.

%package       devel
Summary:       Development files for the libmng
Requires:      %{name} = %{version}-%{release}
Requires:      libjpeg-devel zlib-devel

%description   devel
This package contains development files.

%package       help
Summary:       Help documents for the libmng

%description   help
This package contains help documents.

%prep
%autosetup -p1

%build
cp makefiles/Makefile.am .
autoreconf -if
%configure --enable-shared --with-zlib --with-jpeg --with-gnu-ld --with-lcms2
%make_build

%install
%make_install
%delete_la_and_a

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc doc/* CHANGES README*
%license LICENSE
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%doc doc/*
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libmng.pc

%files help
%defattr(-,root,root)
%{_mandir}/man3/*
%{_mandir}/man5/*
%doc CHANGES README*

%changelog
* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.3-10
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the files of spec

* Tue  Sep 17 2019 Yiru Wang <wangyiru1@huawei.com> - 2.0.3-9
- Pakcage init
